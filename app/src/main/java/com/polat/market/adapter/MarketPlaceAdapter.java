package com.polat.market.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.polat.market.MapsActivity;
import com.polat.market.model.MarketPlace;
import com.polat.market.databinding.RecyclerRowBinding;

import java.util.List;

public class MarketPlaceAdapter extends RecyclerView.Adapter<MarketPlaceHolder> {

    List<MarketPlace> marketPlaceList;

    public MarketPlaceAdapter(List<MarketPlace> marketPlaceList) {
        this.marketPlaceList = marketPlaceList;
    }

    @NonNull
    @Override
    public MarketPlaceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerRowBinding recyclerRowBinding = RecyclerRowBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        return new MarketPlaceHolder(recyclerRowBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MarketPlaceHolder holder, int position) {
        MarketPlace item = marketPlaceList.get(position);
        holder.recyclerRowBinding.recyclerViewTextView.setText(item.name);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), MapsActivity.class);
                intent.putExtra("item",item);
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return marketPlaceList.size();
    }


}
