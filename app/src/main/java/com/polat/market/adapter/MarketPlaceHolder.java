package com.polat.market.adapter;

import androidx.recyclerview.widget.RecyclerView;

import com.polat.market.databinding.RecyclerRowBinding;

public class MarketPlaceHolder extends RecyclerView.ViewHolder{
    RecyclerRowBinding recyclerRowBinding;
    public MarketPlaceHolder(RecyclerRowBinding recyclerRowBinding) {
        super(recyclerRowBinding.getRoot());
        this.recyclerRowBinding = recyclerRowBinding;
    }
}