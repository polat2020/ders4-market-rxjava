package com.polat.market.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.polat.market.model.MarketPlace;

@Database(entities = {MarketPlace.class}, version = 1)
public abstract class MarketPlaceDatabase extends RoomDatabase {
    public abstract MarketPlaceDao placeDao();
}
