package com.polat.market.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.polat.market.model.MarketPlace;

import java.util.List;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Flowable;

@Dao
public interface MarketPlaceDao {

    @Query("SELECT * FROM MarketPlace")
    Flowable<List<MarketPlace>> getAll();

    @Insert
    Completable insert(MarketPlace marketPlace);

    @Delete
    Completable delete(MarketPlace marketPlace);

}
