package com.polat.market.db;

import android.content.Context;

import androidx.room.Room;

public class DbHelper {
    MarketPlaceDatabase db;
    public DbHelper(Context context) {
        db =  Room.databaseBuilder(context,
                MarketPlaceDatabase.class, "Market")
                //.allowMainThreadQueries()
                .build();
    }

    public MarketPlaceDao getPlaceDao(){
        return  db.placeDao();

    }
}
